I. Installation and Running Instructions
----------------------------------------

The base solution and the extensions are in the form of separate Java files. The source code for them can be found under the 'src' folder. 
The solutions can be run using the command line in the following steps -
 
1. Download the latest appropriate Z3 pre-compiled binary from the following link - https://github.com/Z3Prover/z3/releases
 
2. Navigate to 'out/production/Coursework' and set that folder as the current working directory on the terminal. This folder 
consists of the class files that are to be used for running the solutions.  
 
3. If running on a Linux machine, provide a link to the 'bin' folder in the downloaded Z3 folder under 'LD_LIBRARY_PATH'. 
On Windows, add the link to the bin folder to the classpath.
 
4. Add this current directory to the classpath.
 
5. Add all the JAR files under the 'libs' folder to the classpath.
 
6. After specifying the file to run, also provide as the next argument the input board (example, 'smallRectangularBoard.csv') for that file.
 
As an example, this is the command I use to run the base solution on my Ubuntu machine -
 
LD_LIBRARY_PATH="/home/linuxlite/DP/z3/bin" java -cp ".:/home/linuxlite/DP/repo/libs/*:/home/linuxlite/DP/repo/libs/jung/*" Base "/home/linuxlite/DP/repo/smallRectangularBoard.csv"
 
There are 5 solution files in total as follows -
 
1. Base - This file contains the base solution
2. InputToFinalConfig - Extension 2, searches for solutions from a given input configuration to a given final configuration
3. NumberOfSolutions - Extension 3, outputs number of possible solutions
4. LeastSweeps - Extension 4, searches for a solution with the fewest number of sweeps
5. WeightedGame - Extension 6, solves extended weighted game
 
Note that Extension 1 is incorporated as part of all the 5 solutions above. Extension 5 (bit-vectors) was not attempted due to lack of time.

The pre-made board files included are as follows - 

smallTriangleBoard.csv - The 10-hole 'Triangle(4)' board as described here, http://recmath.org/pegsolitaire/tindex.html#boards
smallRectangularBoard.csv - A small 3x4 rectangular board 
rectangularBoard.csv - A bigger 4x6 rectangular board
inputToFinalConfigSmallTriangleBoard.csv - An example of the triangle board for use with InputToFinalConfig
inputToFinalConfigRectangularBoard.csv - An example of the 4x6 rectangle board for use with InputToFinalConfig
weightSmallTriangleBoard.csv - An example of the triangle board for use with WeightedGame
englishBoard.csv - The standard 33 hole English board

For testing purposes these files were run with the corresponding solutions. Here are the results - 

Base
	smallTriangleBoard - 7 move solution found
	smallRectangularBoard - 9 move solution found
	rectangularBoard - 21 move solution found
InputToFinalConfig
	inputToFinalConfigSmallTriangleBoard - 2 solutions found from specified configs
	inputToFinalConfigRectangularBoard - 1 solution found from sepcified configs
NumberOfSolutions
	smallTriangleBoard - 14 solutions found
	smallRectangularBoard - 488 solutions found
LeastSweeps
	smallTriangleBoard - 5 sweeps solution
	smallRectangularBoard - 6 sweeps solution
WeightedGame
	weightSmallTriangleBoard - game satisfies weight constraints
	
II. Game board representation 
-----------------------------

Throughout all solutions the entire board can be thought of as a Cartesian Plane consisting of only the positive
quadrant. Each hole in the board is represented in the form of Cartesian co-ordinates. This is done in the form 
of an ordered pair, where the first ordinate specifies the X co-ordinate while the second one specifies the Y 
co-ordinate, e.g, a hole on the board can be located at point (2,3).

Points that are are at a distance 1 X or 1 Y co-ordinate away from each other are considered to be neighbouring 
points on the board. The implication of this is that a peg in one of these holes can jump over a peg in another. 
For example, suppose there are holes at (3,0), (4,0) and (5,0) and that the first two holes have pegs. Then the 
move [(0,3),(0,4),(0,5)], where the peg in the first hole jumps over the peg in the second to the vacant third hole,
is valid as (4,0) is a neighbour of (3,0), (5,0) is a neighbour of (4,0) and all three holes lie along the same direction. 
Note that diagonal moves are NOT supported by default, although they can be turned on as described later.  

In addition to this, the representation also supports triangular boards through the use of decimal co-ordinates. 
Points that are 1 Y co-ordinate AND 0.5 X co-ordinate away are considered to be neighbours. As an example, the 
hole (0.5,1) is a neighbour of both holes (0,0) and (1,0). Note that diagonal triangular moves such as 
[(0,0),(0.5,1),(1,2)] are considered different to normal diagonal moves and ARE enabled by default. 
  
It is also worth noting that the board holes do NOT necessarily have to start from (0,0). Holes, and indeed the
entire board, can be placed anywhere on the graph.  

III. Input board file format
----------------------------

This section covers the input format of the board files for the base solutions and the separate extensions. 
The input format is largely consistent across all solutions except for slight changes to accommodate special
features/requirements of the individual extensions. 

A. Base solution 

The input format for the base solution serves as the foundation for the inputs of all other solutions as they are
all essentially slight modifications of the base. The input representation is best understood with the help of an 
example. Consider the 3x4 rectangular board represented by 'smallRectangularBoard.csv', the contents of which are as
follows - 

/****************************
2   <-- The file header is the ID of the hole/node that we want empty at the start, i.e, without a peg 
0,0   <-- First hole of the board, assigned an ID of 0   	
1,0		
2,0   <-- IDs are assigned incrementally, so this node has ID 2, which is the node we want empty at start
3,0
0,1
1,1
2,1
3,1
0,2
1,2
2,2
3,2   <-- Final node that has ID 11, thus there are a total of 12 nodes in the 3x4 rectangular board 
****************************/

The base solution is only able to solve 'single vacancy to single survivor (SVSS)' problems and thus we specify the one 
empty hole we want to start off with in the initial board configuration. The position of each node is inputted in the form
of the Cartesian co-ordinates, and all the nodes together form the entire game board. Note that this representation automatically
implements the first extension, where the program is supposed to take any undirected graph input as the board.   

B. Input to final configuration 

For this extension there is no header specifying which hole needs to be empty at the start since the user should have the ability 
to specify whether a peg exists for each individual node. An example of the input is as follows, taken from 
'inputToFinalConfigSmallTriangleBoard.csv'

/****************************
0,0,false,false   <-- This time there is no header, we start off directly with the nodes we want on the board
1,0,false,false
2,0,false,false   <-- the third attribute specifies presence of peg in the given hole in the initial configuration, false means the hole doesn't have a peg while true means it does
3,0,true,false   <-- similarly fourth attribute specifies presence of peg in final configuration
0.5,1,false,false   
1.5,1,true,false   <-- taken together, for this node we specify that it starts off with a peg in the initial configuration but ends up empty in the final config 
2.5,1,true,false
1,2,true,false   <-- The node ID assignments remain the same so this node has an ID of 7
2,2,true,true
1.5,3,false,false
****************************/

C. Number of solutions 

This extension uses the exact same input format as the BASE solution, and is also only able to deal with SVSS problems.  

D. Least sweeps 

This extension uses the exact same input format as the BASE solution, and is also only able to deal with SVSS problems.  

E. Weighted game 

The input format for this extension is largely the same as the base solution, the only difference being the addition of weights.
Consider a one-dimensional board as follows - 

/****************************
2   <-- The header specifying which node we want empty at the start 
0,0,10   <-- The third attribute specifies the initial weight the node should have
1,0,5
2,0,0   <-- By convention the empty node should be assigned an initial weight of 0 (although in practice assigning any weight is fine as it doesn't make a difference) 
****************************/

This solution is also designed only to deal with SVSS problems. 

IV. Solution design overview
----------------------------

This section will briefly summarise the base solution and the extensions, and will also provide an insight and justifications of
certain design decisions taken. 

A. Base solution 

Disclosure - While the overall approach to this solution was mostly developed independently by me, I referred to an online tutorial regarding
CNF peg solitaire by Stanford and incorporated certain aspects of their approach. The 'selection variables' part in particular is an aspect of
their approach that I wouldn't have been able to come up with myself. However, all the code is 100% mine and I have not even looked at the actual code
that they have provided on their webpage. I've only read up on parts of their approach and implemented them independently by myself. The tutorial in
question is available here - http://ai.stanford.edu/~chuongdo/satpage/

The base solution can be broken up into a series of distinct parts as follows - 

1. Creating graph from input file - The board input file created by the user is read by the program and is then internally represented 
as a directed graph. Note that to the user, the input is still in the form of an undirected graph, the program infers relative directions
from the specified Cartesian co-ordinates. For the graph itself, an external Java library called JUNG is made use of. While a custom graph
could have been created and used, leveraging an existing library made more sense as it provided the required functionality right out of the
box and allowed focus to be maintained on solving the actual game. 

Each input hole/node in the file is represented as a separate node in the JUNG graph, and edges are created between nodes that are neighbours. 
There are always two edges between a node and its neighbour, one outgoing directed edge containing the direction of the neighbour relative to the
node and one incoming edge containing the direction of the node relative to the neighbour. Once the neighbours and directions are inferred there
is no need to keep track of the actual co-ordinates any longer and the created graph by itself represents the entire game board. 

An alternate input method was considered where the user themselves manually specified the edges between nodes and the relative directions to
neighbours. While it would eliminate all the pre-processing with regards to inferring directions from co-ordinates, it was deemed to be too
cumbersome and error prone for the user to actually create the input graph. Thus the co-ordinate method strikes a balance between user convenience
and obtaining the required information.  

2. Inferring possible board moves from graph - The next step was to obtain a collection of all the possible moves that could be made on the board.
As we haven't taken any pegs into consideration yet, a possible move in this case is regarded as a trio of nodes that exist in the same direction. 
In our graph, these moves can be identified by looking at the outgoing edge between a node and a neighbour, and then the outgoing edge between this 
neighbour and one of its neighbours. If the direction in both these outgoing edges is the same then it is counted as a possible move. 

While searching for all possible moves could have been made more efficient by using a custom graph representation, using a library like JUNG allows 
us to iterate over the graph and search for these possible moves using the standard functions provided by the library, and thus makes implementation
easier and more understandable. As this is still part of pre-processing it does not adversely affect overall performance. 

3. Representing and creating board 'states' - A board state can be considered as a configuration of the board at that given time. Configuration here
refers to specifying the presence or absence of a peg for each individual node. Configurations are defined in the form of Z3 boolean variables, where 
variable Nij represents presence of peg in node j in board state i. True implies that the hole has a peg, while false implies that it is empty. 

Given that the base solution deals only with the SVSS problem we can derive the number of board states required. Each transition from one board state to
another corresponds to a move, which in turn leads to removal of one peg from the game. Thus the number of transitions required is 
(no. of starting pegs - no. of final remaining ones). As the SVSS problem always starts with one hole empty and ends with one peg remaining, the number 
of transitions is [(no. of holes on board - 1) - 1]. We also need an additional state to represent the initial configuration, so the number of board states
is the (total no. of holes - 1). Thus we declare (n-1)*n boolean variables, where n is the total no. of holes and where each board state has a boolean variable 
for each individual hole.  

4. Defining 'valid' moves - We have a collection of all possible moves that can be made on the board at any time, but all these moves might not be valid for a
given configuration. Determining whether a move is valid depends on the positions of the pegs at that given time. We know that a move can be made in the game 
if three nodes are in the same direction and the first two have pegs with the last one being empty. We also know that a move essentially corresponds to a
transition from one board state to another. Thus using logic, we can define a move (i,j,k), where the peg in node i jumps over peg in node j and lands
in empty spot in node k, from board state p to q to be valid if Npi and Npj are true, and Nqi and Nqj are false. Additionally, Npk should be false while Nqk should
be true. Finally, variables corresponding to all other holes should remain unchanged between the two states. 

5. Defining 'legal' transitions - Once we have a notion of a valid move, we know that a transition from one board state to another is possible only if there is 
a valid move available to take. Thus an expression for a legal transition is formed by simply taking a disjunction as follows - 

	legal(p,q) = valid(0,1,2) | valid(5,6,7) | ... | valid(i,j,k)

where each move we check validity for is taken from the collection of all possible moves that can be made on the board. 

6. Checking for game solution - Once we decide on what constitutes a legal transitions, since we already have the number of required transitions for a SVSS game we
can check for a game solution by checking whether each transition from state 0 to 1, 1 to 2...(no. of nodes-2) to (no. of nodes-1) is legal. In logic, this is 
represented in the form of a conjunction of all these legal transitions. This conjunction forms the final CNF expression that we feed to the SAT solver. 

7. Concept of 'selection' variables - The expression for a legal transition simply checks whether there is a valid move available to make the transition possible. In 
case there are multiple valid moves possible, currently it does not actually select one to go ahead with. Simulating selection of a single move as would be done in an 
actual game is achieved by introducing new boolean 'selection' variables for each transition. Unique combinations of these selection variables are associated with each 
different move in the legal transition expression's disjunction. It is designed so that once the SAT solver assigns truth values to each variable in the expression, the 
values assigned to the selection variables results in all the conditions for one of the valid moves to become true, which in essence means selecting that particular move. 
This method is covered in greater detail in the Stanford approach in the third point under the 'Encoding peg solitaire in Conjunctive Normal Form (CNF)' section. 

8. Inferring game moves - If the SAT solver returns a satisfying assignment for the game, the individual moves that constitute the solution to the game are inferred by 
going through each board state transition one by one and searching for boolean variables that have changed from true to false or vice versa since a move would result in
two nodes changing from having pegs to being empty while one would change from being empty to containing a peg. The node being jumped can be identified by checking for a
(true -> false) node that has an edge with the single (false -> true) node, and thus by elimination the jumping node can also be identified. 

Note that by default normal diagonal moves are not taken into consideration as standard peg solitaire on the English board does not allow them. However, in each solution (including
the base one) diagonal moves can be turned on by manually going to the 'createGameBoardFromFile' function in the code and uncommenting the section containing 'neighbour UP-LEFT', 
'neighbour DOWN-LEFT', 'neighbour UP-RIGHT' and 'neighbour DOWN-RIGHT'. A comment is included next to this section along with the necessary instruction to make it easy to detect. 

B. Input to final configuration 

The fundamental method followed is largely the same as the base solution. Additional constraints are introduced in the solver to set the values of the boolean variables 
in the initial and final board states to be the values inputted by the user. Furthermore, as it is not necessary that the user will enter configurations corresponding to
a SVSS problem we determine the number of board states required as [(no. of pegs in initial user config) - (no. of pegs in final user config) + 1]. 

In addition, as the extension requires having to find ALL possible solutions for the given configuration, the solver is run in iterations. Each iteration results in a different
solution by introducing additional constraints at the end of each iteration that prohibits the solver from allocating the exact same assignments that resulted in the most recent 
solution. Each such assignment removes the possibility of any of the past solutions being proposed again so the solver keeps finding new ones until all possible ones are exhausted. 
As these constraints are all appended to the original expression, the requirement of the initial and final states being those specified by the user remains intact. 

C. Number of solutions 

This solution is achieved by simply taking the base solution and running it in iterations as described above. Each new solution is counted and the sum total is displayed to the user
after all solutions are exhausted. 

D. Least sweeps 

Determining the solution with the least number of sweeps again requires considering every possible solution to find one that produces the least sweeps. Hence the solution was implemented
by iterating through each solution in the manner described above and calculating the number of sweeps achieved by the solution. A record is kept of the least number of sweeps seen till now, 
so when a solution with number of sweeps lower than that is spotted the record is updated and the model that produced that solution is also stored in order to reproduce the corresponding moves
at the end. 

The start/end of a sweep is identified by keeping track of the currently jumping peg as it moves across nodes. If the same peg keeps jumping consecutively, it means that its sweep is being 
continued. The moment another peg jumps instead is when the previous peg's sweep ends, and we begin keeping track of the new peg that just jumped. 

E. Weighted game 

This extension is also solved by iterating through all possible solutions (without considering weights at first), and then checking whether each move in the current obtained solution 
satisfies the weight constraint of the jumping node having a weight greater than its neighbour. As soon as a solution is found where each move adheres to this constraint, the loop is 
broken and the solution is returned to the user. 

One might understandably think that it would be more efficient to get each move for a solution one by one and checking if it adheres to the weight constraint instead of getting all the moves
at once first and then going through each one individually. Unfortunately, getting each move incrementally is not possible with the method we use. A legal transition from one board state to the
other only ensures that the actual move corresponding to the transition obeys the rule of a peg jumping over another to land in an empty space. What it DOESN'T ensure is that on carrying out this 
move we will eventually end up in a final configuration where only one peg is left. This guarantee is achieved only by also taking further legal moves down the line into consideration in the CNF expression,
which is why it is only possible to get all moves at once for the solution instead of going through the moves incrementally. 