import com.microsoft.z3.*;
import com.opencsv.CSVReader;
import edu.uci.ics.jung.graph.DirectedSparseMultigraph;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;

enum Position {
    // up, down, left, right,
    // up-left, down-left, up-right, down-right
    // up-left-triangle, down-left-triangle, up-right-triangle, down-right-triangle
    U, D, L, R, UL, DL, UR, DR, ULT, DLT, URT, DRT
}

public class Base {

    static String boardFileName;  // name of the board file to use
    static HashMap<Integer, Node> nodeMap;   // hashmap to access nodes in the graph by number
    static int emptyHoleNodeNumber = -1;    // variable to store which node number user wants empty at the start

    public static void main(String[] args) throws Exception {
        Context context = new Context();

        boardFileName = args[0];

        DirectedSparseMultigraph<Node, Edge> gameBoard = createGameBoardFromFile();
        ArrayList<Move> allPossibleMoves = getAllPossibleMovesOnBoard(gameBoard);
        BoolExpr[][] boardStates = createBoardStates(gameBoard, context);

        BoolExpr finalCnfExpression = context.mkTrue(); // need to initialise to something, start with T since (A and T) == A

        // legal[board state x, board state (x+1)]
        for(int i = 0; i < boardStates.length-1; i++) {
            finalCnfExpression = context.mkAnd(finalCnfExpression, createLegalTransitionExpression(i, i+1, boardStates[i], boardStates[i+1], allPossibleMoves, context));
        }

        System.out.println("Beginning SAT solver");

        Solver solver = context.mkSolver();
        solver.add(finalCnfExpression);
        solver.add(context.mkNot(boardStates[0][emptyHoleNodeNumber])); // make the node that user specifies empty in the initial board state
        System.out.println(solver.check());

        if(solver.check().equals(Status.UNSATISFIABLE)) {
            System.out.println("No solution exists for this board configuration");
            System.exit(0);
        }

        Model model = solver.getModel();

        for(int i = 0; i < boardStates.length-1; i++) {
            int numberOfNodes = boardStates[i].length;

            Node falseToTrueNode = null;
            ArrayList<Node> trueToFalseNodes = new ArrayList<Node>();

            for(int j = 0; j < numberOfNodes; j++) {
                Expr initialBoardStateAssignment = model.getConstInterp(boardStates[i][j]);
                Expr newBoardStateAssignment = model.getConstInterp(boardStates[i+1][j]);

                // want to give the very first initial board state assignment to the user
                if(i == 0) {
                    if(j == 0) {
                        System.out.println("Initial board assignments - ");
                    }

                    if(initialBoardStateAssignment.equals(context.mkTrue())) {
                        System.out.print("Hole " + j + " has peg, ");
                    } else {
                        System.out.print("Hole " + j + " is empty, ");
                    }

                    if(j == numberOfNodes - 1) {
                        System.out.println();
                        System.out.println("The game moves are as follows - ");
                    }
                }

                // nodes that don't have same assignments across old and new states are involved in the move for this turn
                if(!initialBoardStateAssignment.equals(newBoardStateAssignment)) {
                    if(initialBoardStateAssignment.equals(context.mkFalse()) && newBoardStateAssignment.equals(context.mkTrue())) {
                        falseToTrueNode = nodeMap.get(j);
                    } else {
                        trueToFalseNodes.add(nodeMap.get(j));
                    }
                }
            }

            for(int x = 0; x < trueToFalseNodes.size(); x++) {
                // if there doesn't exist an edge between the two nodes, it means that this current node jumps over the other node in the arraylist and lands in the 'falseToTrueNode'
                if(!gameBoard.isNeighbor(trueToFalseNodes.get(x), falseToTrueNode)) {
                    System.out.println("Move " + i + " : (" + trueToFalseNodes.get(x).getNumber() + "," + trueToFalseNodes.get(1 - x).getNumber() + "," + falseToTrueNode.getNumber() + ")");
                    // the (1 - x) bit returns index 1 when input is 0, and index 0 when input is 1 (http://stackoverflow.com/questions/2411023/most-elegant-way-to-change-0-to-1-and-vice-versa)
                }
            }
        }

    }

    public static DirectedSparseMultigraph<Node, Edge> createGameBoardFromFile() throws Exception {
        DirectedSparseMultigraph<Node, Edge> gameBoard = new DirectedSparseMultigraph<Node, Edge>();

        CSVReader reader = new CSVReader(new FileReader(boardFileName));

        // hashmap where key is the string representation of board point, e.g "2,3", and value is the number of the node
        HashMap<String, Integer> userInputBoard = new HashMap<String, Integer>();

        // global map through which node number can be used to get the actual node
        nodeMap = new HashMap<Integer, Node>();
        // counter that assigns an incremental node number to each node encountered in the input file
        int nodeNumber = 0;

        String[] nextLine;
        nextLine = reader.readNext();   // reads the header of the CSV file which contains the number of the node the user sets to empty
        emptyHoleNodeNumber = Integer.parseInt(nextLine[0]);

        // iterate rest of the file till we reach the end
        while((nextLine = reader.readNext()) != null) {
            double boardPointXCoordinate = Double.parseDouble(nextLine[0]);
            double boardPointYCoordinate = Double.parseDouble(nextLine[1]);

            // convert to string representation to use as key for 'userInputBoard' hashmap
            String boardPointCoordinates = boardPointXCoordinate + "," + boardPointYCoordinate;
            userInputBoard.put(boardPointCoordinates, nodeNumber);

            Node node = new Node(nodeNumber, -1);   // we don't use weights in this version so set it to a negative number
            gameBoard.addVertex(node);
            nodeMap.put(nodeNumber, node);
            nodeNumber++;
        }

        // once we've gone through input file, iterate all the string nodes that have been added by the user
        for(String boardPointCoordinates : userInputBoard.keySet()) {
            String[] boardPointSplitCoordinates = boardPointCoordinates.split(",");
            double boardPointXCoordinate = Double.parseDouble(boardPointSplitCoordinates[0]);
            double boardPointYCoordinate = Double.parseDouble(boardPointSplitCoordinates[1]);

            // variable to specify the different 8-neighbours of the current node
            String boardNeighbourPointCoordinates;

            // neighbour UP
            boardNeighbourPointCoordinates = boardPointXCoordinate + "," + (boardPointYCoordinate + 1);
            addEdgeToGameBoard(boardPointCoordinates, boardNeighbourPointCoordinates, Position.U, userInputBoard, gameBoard);

            // neighbour DOWN
            boardNeighbourPointCoordinates = boardPointXCoordinate + "," + (boardPointYCoordinate - 1);
            addEdgeToGameBoard(boardPointCoordinates, boardNeighbourPointCoordinates, Position.D, userInputBoard, gameBoard);

            // neighbour LEFT
            boardNeighbourPointCoordinates = (boardPointXCoordinate - 1) + "," + boardPointYCoordinate;
            addEdgeToGameBoard(boardPointCoordinates, boardNeighbourPointCoordinates, Position.L, userInputBoard, gameBoard);

            // neighbour RIGHT
            boardNeighbourPointCoordinates = (boardPointXCoordinate + 1) + "," + boardPointYCoordinate;
            addEdgeToGameBoard(boardPointCoordinates, boardNeighbourPointCoordinates, Position.R, userInputBoard, gameBoard);

            // these are the diagonal edges that are disabled by default, but can be included by simply uncommenting the section below

           /* // neighbour UP-LEFT
            boardNeighbourPointCoordinates = (boardPointXCoordinate - 1) + "," + (boardPointYCoordinate + 1);
            addEdgeToGameBoard(boardPointCoordinates, boardNeighbourPointCoordinates, Position.UL, userInputBoard, gameBoard);

            // neighbour DOWN-LEFT
            boardNeighbourPointCoordinates = (boardPointXCoordinate - 1) + "," + (boardPointYCoordinate - 1);
            addEdgeToGameBoard(boardPointCoordinates, boardNeighbourPointCoordinates, Position.DL, userInputBoard, gameBoard);

            // neighbour UP-RIGHT
            boardNeighbourPointCoordinates = (boardPointXCoordinate + 1) + "," + (boardPointYCoordinate + 1);
            addEdgeToGameBoard(boardPointCoordinates, boardNeighbourPointCoordinates, Position.UR, userInputBoard, gameBoard);

            // neighbour DOWN-RIGHT
            boardNeighbourPointCoordinates = (boardPointXCoordinate + 1) + "," + (boardPointYCoordinate - 1);
            addEdgeToGameBoard(boardPointCoordinates, boardNeighbourPointCoordinates, Position.DR, userInputBoard, gameBoard);*/

            // neighbour UP-LEFT-TRIANGLE
            boardNeighbourPointCoordinates = (boardPointXCoordinate - 0.5) + "," + (boardPointYCoordinate + 1);
            addEdgeToGameBoard(boardPointCoordinates, boardNeighbourPointCoordinates, Position.ULT, userInputBoard, gameBoard);

            // neighbour DOWN-LEFT-TRIANGLE
            boardNeighbourPointCoordinates = (boardPointXCoordinate - 0.5) + "," + (boardPointYCoordinate - 1);
            addEdgeToGameBoard(boardPointCoordinates, boardNeighbourPointCoordinates, Position.DLT, userInputBoard, gameBoard);

            // neighbour UP-RIGHT-TRIANGLE
            boardNeighbourPointCoordinates = (boardPointXCoordinate + 0.5) + "," + (boardPointYCoordinate + 1);
            addEdgeToGameBoard(boardPointCoordinates, boardNeighbourPointCoordinates, Position.URT, userInputBoard, gameBoard);

            // neighbour DOWN-RIGHT-TRIANGLE
            boardNeighbourPointCoordinates = (boardPointXCoordinate + 0.5) + "," + (boardPointYCoordinate - 1);
            addEdgeToGameBoard(boardPointCoordinates, boardNeighbourPointCoordinates, Position.DRT, userInputBoard, gameBoard);
        }

        return gameBoard;
    }

    public static void addEdgeToGameBoard(String boardPointCoordinates, String boardNeighbourPointCoordinates, Position neighbourPosition, HashMap<String, Integer> userInputBoard, DirectedSparseMultigraph<Node, Edge> gameBoard) {
        // only want to do anything if the direction we're looking in actually has a neighbour node
        if(userInputBoard.containsKey(boardNeighbourPointCoordinates)) {
            Node currentNode = nodeMap.get(userInputBoard.get(boardPointCoordinates));
            Node neighbourNode = nodeMap.get(userInputBoard.get(boardNeighbourPointCoordinates));

            // add new directed edge from currentNode to neighbourNode with direction of neighbourNode relative to currentNode
            Edge edge = new Edge(neighbourPosition);
            gameBoard.addEdge(edge, currentNode, neighbourNode);
        }
    }

    public static ArrayList<Move> getAllPossibleMovesOnBoard(DirectedSparseMultigraph<Node, Edge> gameBoard) {

        ArrayList<Move> allPossibleMoves = new ArrayList<Move>();

        System.out.println("The possible moves on this board are - ");

        // looks for combinations of nodes, the node's neighbours and the neighbour's neighbours
        // any combination for which all three of them are in the same direction is a possible valid move on the given board
        for(Node initialNode : gameBoard.getVertices()) {
            for(Node neighbourNode : gameBoard.getNeighbors(initialNode)) {
                Edge edge = gameBoard.findEdge(initialNode, neighbourNode);
                Position nodePosition = edge.getNodePosition();
                for(Node destinationNode : gameBoard.getNeighbors(neighbourNode)) {
                    Edge destinationEdge = gameBoard.findEdge(neighbourNode, destinationNode);
                    if(destinationEdge.getNodePosition() == nodePosition) {
                        allPossibleMoves.add(new Move(initialNode.getNumber(), neighbourNode.getNumber(), destinationNode.getNumber()));
                        System.out.println("(" + initialNode.getNumber() + "," + neighbourNode.getNumber() + "," + destinationNode.getNumber() + ") : " + nodePosition);
                    }
                }
            }
        }

        return allPossibleMoves;
    }

    public static BoolExpr[][] createBoardStates(DirectedSparseMultigraph<Node, Edge> gameBoard, Context context) throws Exception {
        int numberOfNodes = gameBoard.getVertexCount();

        BoolExpr[][] boardStates = new BoolExpr[numberOfNodes-1][numberOfNodes]; // n-1 states of length n each, corresponding to each hole in board
        // n-1 states because we have **N-1 PEGS (assumption)**, and thus need N-1 states for the situation of only one peg remaining
        for(int i = 0; i < boardStates.length; i++) {
            for(int j = 0; j < boardStates[i].length; j++) {
                boardStates[i][j] = context.mkBoolConst("Hole " + j + " in board state " + i);
            }
        }

        return boardStates;
    }

    // valid(p, q, (x, y, z)), where (x, y, z) is the move from state p to state q
    // uses approach described in http://ai.stanford.edu/~chuongdo/satpage/
    public static BoolExpr createValidMoveExpression(BoolExpr[] initialBoardState, BoolExpr[] finalBoardState, Move move, BoolExpr selectionVariableCombination, Context context) throws  Exception {

        BoolExpr validMoveExpression = context.mkOr(
                selectionVariableCombination,   // selectionVariableCombination is OR'd with each 'AND' component of the 'valid' expression
                context.mkAnd(initialBoardState[move.getInitial()], context.mkNot(finalBoardState[move.getInitial()])) // p_x & !q_x
        );

        // append new expression to the overall one
        validMoveExpression = context.mkAnd(
                validMoveExpression,
                context.mkOr(
                        selectionVariableCombination,
                        context.mkAnd(initialBoardState[move.getNeighbour()], context.mkNot(finalBoardState[move.getNeighbour()]))  // add p_y & !q_y
                )
        );

        validMoveExpression = context.mkAnd(
                validMoveExpression,
                context.mkOr(
                        selectionVariableCombination,
                        context.mkAnd(context.mkNot(initialBoardState[move.getDestination()]), finalBoardState[move.getDestination()])  // add !p_z & q_z
                )
        );

        for(int i = 0; i < initialBoardState.length; i++) {
            if(i == move.getInitial() || i == move.getNeighbour() || i == move.getDestination()) {
                continue;
            }

            // (p iff q) = (!p_i || q_i) & (p_i || !q_i)
            BoolExpr iffExpression = context.mkAnd(
                    context.mkOr(context.mkNot(initialBoardState[i]), finalBoardState[i]),
                    context.mkOr(initialBoardState[i], context.mkNot(finalBoardState[i]))
            );

            validMoveExpression = context.mkAnd(
                    validMoveExpression,
                    context.mkOr(selectionVariableCombination, iffExpression)
            );
        }

        return validMoveExpression;
    }

    public static ArrayList<BoolExpr> generateSelectionVariableCombinations(ArrayList<Move> allPossibleMoves, int initialBoardState, int finalBoardState, Context context) throws Exception {
        int numberOfPossibleMoves = allPossibleMoves.size();    // number of selection variables is determined by the total number of possible moves that can be made
        int numberOfSelectionVariables = logBase2(numberOfPossibleMoves);   // rounded up in custom method to give required number
        ArrayList<BoolExpr> selectionVariables = new ArrayList<BoolExpr>();
        ArrayList<BoolExpr> selectionVariableCombinations = new ArrayList<BoolExpr>();

        // instantiate selection variables for this transition
        for(int i = 0; i < numberOfSelectionVariables; i++) {
            BoolExpr selectionVariable = context.mkBoolConst("Selection variable " + i + " for board state " + initialBoardState + " -> " + finalBoardState);
            selectionVariables.add(selectionVariable);
        }

        // number of combinations of n boolean variables : 0 to (2^n) - 1
        for(int i = 0; i < Math.pow(2, numberOfSelectionVariables); i++) {
            // http://stackoverflow.com/questions/1316765/what-is-an-efficient-algorithm-to-create-all-possible-combinations
            // http://stackoverflow.com/questions/8457734/pad-a-binary-string-equal-to-zero-0-with-leading-zeros-in-java
            String booleanCombination = String.format("%" + numberOfSelectionVariables + "s", Integer.toBinaryString(i)).replace(' ', '0');
            BoolExpr selectionVariableCombination = context.mkFalse();  // need to initialise variable to something, make it false since (X v F) = X
            // go through each bit of the generated combination
            for(int j = 0; j < booleanCombination.length(); j++) {
                // get the selection variable corresponding to the current bit
                BoolExpr selectionVariable = selectionVariables.get(j);
                int bitValue = Character.getNumericValue(booleanCombination.charAt(j));
                // if bit in current combination is 0, use the boolean variable, else use the negation
                if(bitValue == 0) {
                    selectionVariableCombination = context.mkOr(selectionVariableCombination, selectionVariable);
                } else if(bitValue == 1) {
                    selectionVariableCombination = context.mkOr(selectionVariableCombination, context.mkNot(selectionVariable));
                }
            }
            selectionVariableCombinations.add(selectionVariableCombination);
        }

        return selectionVariableCombinations;
    }

    public static BoolExpr createLegalTransitionExpression(int initialBoardStateNumber, int finalBoardStateNumber, BoolExpr[] initialBoardState, BoolExpr[] finalBoardState, ArrayList<Move> allPossibleMoves, Context context) throws Exception {
        // generate the selection variable combinations for this board transition
        ArrayList<BoolExpr> selectionVariableCombinations = generateSelectionVariableCombinations(allPossibleMoves, initialBoardStateNumber, finalBoardStateNumber, context);

        BoolExpr legalTransitionExpression = context.mkTrue();  // (X and T) = X

        for(int i = 0; i < allPossibleMoves.size(); i++) {
            Move move = allPossibleMoves.get(i);
            // choose one unique selection variable to associate with the current 'valid' expression
            BoolExpr selectionVariableCombination = selectionVariableCombinations.get(i);
            legalTransitionExpression = context.mkAnd(legalTransitionExpression, createValidMoveExpression(initialBoardState, finalBoardState, move, selectionVariableCombination, context));
        }

        // all the unused selection variable combinations need to be included in expression as well
        for(int i = allPossibleMoves.size(); i < selectionVariableCombinations.size(); i++) {
            legalTransitionExpression = context.mkAnd(legalTransitionExpression, selectionVariableCombinations.get(i));
        }

        return legalTransitionExpression;
    }

    public static int logBase2(double number) {
        return (int) Math.ceil(Math.log(number)/Math.log(2));
    }
}

class Move {
    private int initial;
    private int neighbour;
    private int destination;

    public Move(int initial, int neighbour, int destination) {
        this.setInitial(initial);
        this.setNeighbour(neighbour);
        this.setDestination(destination);
    }

    public int getInitial() {
        return initial;
    }

    public void setInitial(int initial) {
        this.initial = initial;
    }

    public int getNeighbour() {
        return neighbour;
    }

    public void setNeighbour(int neighbour) {
        this.neighbour = neighbour;
    }

    public int getDestination() {
        return destination;
    }

    public void setDestination(int destination) {
        this.destination = destination;
    }
}

class Edge {
    private Position nodePosition;

    public Edge(Position nodePosition) {
        this.setNodePosition(nodePosition);
    }

    public Position getNodePosition() {
        return nodePosition;
    }

    public void setNodePosition(Position nodePosition) {
        this.nodePosition = nodePosition;
    }
}

class Node {
    private int number;
    private double weight;

    public Node(int number, double weight) {
        this.setNumber(number);
        this.setWeight(weight);
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
