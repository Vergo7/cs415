/**
 * Created by Varun on 01/01/2016.
 */
import com.microsoft.z3.*;
import com.opencsv.CSVReader;
import edu.uci.ics.jung.graph.DirectedSparseMultigraph;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;

public class InputToFinalConfig {

    static String boardFileName;  // name of the board file to use
    static HashMap<Integer, Node> nodeMap;   // hashmap to access nodes in the graph by number
    static ArrayList<BoolExpr> userDefinedConfigConstraints; // constraints that have to be added at the end for carrying out the user defined initial and final configs

    static ArrayList<BoolExpr> selectionVariablesArray = new ArrayList<BoolExpr>(); // arraylist that stores the selection variables used in creating the final CNF expression

    static Model model = null;

    public static void main(String[] args) throws Exception {
        Context context = new Context();

        boardFileName = args[0];

        DirectedSparseMultigraph<Node, Edge> gameBoard = createGameBoardFromFile();
        ArrayList<Move> allPossibleMoves = getAllPossibleMovesOnBoard(gameBoard);
        BoolExpr[][] boardStates = createBoardStates(gameBoard, context);

        BoolExpr finalCnfExpression = context.mkTrue();

        // legal[board state x, board state (x+1)]
        for(int i = 0; i < boardStates.length-1; i++) {
            finalCnfExpression = context.mkAnd(finalCnfExpression, createLegalTransitionExpression(i, i+1, boardStates[i], boardStates[i+1], allPossibleMoves, context));
        }

        Solver solver = context.mkSolver();
        solver.add(finalCnfExpression);
        // add the constraints corresponding to the initial and final board configurations specified by the user
        for(BoolExpr userConstraint : userDefinedConfigConstraints) {
            solver.add(userConstraint);
        }

        while(true) {
            boolean areThereSolutionsLeft = createGameSolution(solver, boardStates, gameBoard, context);
            if(areThereSolutionsLeft == false) {
                System.out.println("All solutions for the given initial and final configurations have been iterated through");
                break;
            }

            System.out.println("Moving on to the next solution...");
            BoolExpr excludeSolutionExpression = context.mkFalse(); // (A v F) == A

            for(BoolExpr[] boardState : boardStates) {
                for(BoolExpr holeBoolVariable : boardState) {
                    // we want to EXCLUDE the assignments that the model made in this iteration
                    // true -> not(variable) and false -> variable in this case is because of De Morgan's rule
                    // e.g. not(a == true and b == false) => not(a) or b
                    // http://stackoverflow.com/questions/13395391/z3-finding-all-satisfying-models
                    if(model.getConstInterp(holeBoolVariable).equals(context.mkTrue())) {
                        excludeSolutionExpression = context.mkOr(excludeSolutionExpression, context.mkNot(holeBoolVariable));
                    } else {
                        excludeSolutionExpression = context.mkOr(excludeSolutionExpression, holeBoolVariable);
                    }
                }
            }

            // apply the same exclusions for the selection variables
            for(BoolExpr selectionVariable : selectionVariablesArray) {
                if(model.getConstInterp(selectionVariable).equals(context.mkTrue())) {
                    excludeSolutionExpression = context.mkOr(excludeSolutionExpression, context.mkNot(selectionVariable));
                } else {
                    excludeSolutionExpression = context.mkOr(excludeSolutionExpression, selectionVariable);
                }
            }

            solver.add(excludeSolutionExpression);
        }
    }

    public static boolean createGameSolution(Solver solver, BoolExpr[][] boardStates, DirectedSparseMultigraph<Node, Edge> gameBoard, Context context) throws Exception {
        System.out.println("Beginning SAT solver");
        System.out.println(solver.check());

        if(solver.check().equals(Status.UNSATISFIABLE)) {
            return false; // no more moves left to iterate through
        }

        model = solver.getModel();

        int numberOfNodes = gameBoard.getVertexCount();
        for(int i = 0; i < boardStates.length-1; i++) {

            Node falseToTrueNode = null;
            ArrayList<Node> trueToFalseNodes = new ArrayList<Node>();

            for(int j = 0; j < numberOfNodes; j++) {
                Expr initialBoardStateAssignment = model.getConstInterp(boardStates[i][j]);
                Expr newBoardStateAssignment = model.getConstInterp(boardStates[i+1][j]);

                // want to give the very first initial board state assignment to the user
                if(i == 0) {
                    if(j == 0) {
                        System.out.println("Initial board assignments - ");
                    }

                    if(initialBoardStateAssignment.equals(context.mkTrue())) {
                        System.out.print("Hole " + j + " has peg, ");
                    } else {
                        System.out.print("Hole " + j + " is empty, ");
                    }

                    if(j == numberOfNodes - 1) {
                        System.out.println();
                        System.out.println("The game moves are as follows - ");
                    }
                }

                if(!initialBoardStateAssignment.equals(newBoardStateAssignment)) {
                    if(initialBoardStateAssignment.equals(context.mkFalse()) && newBoardStateAssignment.equals(context.mkTrue())) {
                        falseToTrueNode = nodeMap.get(j);
                    } else {
                        trueToFalseNodes.add(nodeMap.get(j));
                    }
                }
            }

            for(int x = 0; x < trueToFalseNodes.size(); x++) {
                // if there doesn't exist an edge between the two nodes, it means that this current node jumps over the other node in the arraylist and lands in the 'falseToTrueNode'
                if(!gameBoard.isNeighbor(trueToFalseNodes.get(x), falseToTrueNode)) {
                    System.out.println("Move " + i + " : (" + trueToFalseNodes.get(x).getNumber() + "," + trueToFalseNodes.get(1 - x).getNumber() + "," + falseToTrueNode.getNumber() + ")");
                    // the (1 - x) bit returns index 1 when input is 0, and index 0 when input is 1 (http://stackoverflow.com/questions/2411023/most-elegant-way-to-change-0-to-1-and-vice-versa)
                }
            }
        }

        // in this case we also want to give the final board state assignment to the user for them to verify
        for(int j = 0; j < numberOfNodes; j++) {
            Expr finalBoardStateAssignment = model.getConstInterp(boardStates[boardStates.length-1][j]);

            if(j == 0) {
                System.out.println("Final board assignments - ");
            }

            if(finalBoardStateAssignment.equals(context.mkTrue())) {
                System.out.print("Hole " + j + " has peg, ");
            } else {
                System.out.print("Hole " + j + " is empty, ");
            }
        }

        System.out.println("");
        return true;
    }

        public static DirectedSparseMultigraph<Node, Edge> createGameBoardFromFile() throws Exception {
        DirectedSparseMultigraph<Node, Edge> gameBoard = new DirectedSparseMultigraph<Node, Edge>();

        CSVReader reader = new CSVReader(new FileReader(boardFileName));

        // hashmap where key is the string representation of board point, e.g "2,3", and value is the number of the node
        HashMap<String, Integer> userInputBoard = new HashMap<String, Integer>();

        // global map through which node number can be used to get the actual node
        nodeMap = new HashMap<Integer, Node>();
        // counter that assigns an incremental node number to each node encountered in the input file
        int nodeNumber = 0;
        String[] nextLine;
        // iterate file till we reach the end
        while((nextLine = reader.readNext()) != null) {
            double boardPointXCoordinate = Double.parseDouble(nextLine[0]);
            double boardPointYCoordinate = Double.parseDouble(nextLine[1]);

            // convert to string representation to use as key for 'userInputBoard' hashmap
            String boardPointCoordinates = boardPointXCoordinate + "," + boardPointYCoordinate;
            userInputBoard.put(boardPointCoordinates, nodeNumber);

            Node node = new Node(nodeNumber, -1);    // we don't use weights in this version so set it to a negative number
            gameBoard.addVertex(node);
            nodeMap.put(nodeNumber, node);
            nodeNumber++;
        }

        // once we've gone through input file, iterate all the string nodes that have been added by the user
        for(String boardPointCoordinates : userInputBoard.keySet()) {
            String[] boardPointSplitCoordinates = boardPointCoordinates.split(",");
            double boardPointXCoordinate = Double.parseDouble(boardPointSplitCoordinates[0]);
            double boardPointYCoordinate = Double.parseDouble(boardPointSplitCoordinates[1]);

            // variable to specify the different 8-neighbours of the current node
            String boardNeighbourPointCoordinates;

            // neighbour UP
            boardNeighbourPointCoordinates = boardPointXCoordinate + "," + (boardPointYCoordinate + 1);
            addEdgeToGameBoard(boardPointCoordinates, boardNeighbourPointCoordinates, Position.U, userInputBoard, gameBoard);

            // neighbour DOWN
            boardNeighbourPointCoordinates = boardPointXCoordinate + "," + (boardPointYCoordinate - 1);
            addEdgeToGameBoard(boardPointCoordinates, boardNeighbourPointCoordinates, Position.D, userInputBoard, gameBoard);

            // neighbour LEFT
            boardNeighbourPointCoordinates = (boardPointXCoordinate - 1) + "," + boardPointYCoordinate;
            addEdgeToGameBoard(boardPointCoordinates, boardNeighbourPointCoordinates, Position.L, userInputBoard, gameBoard);

            // neighbour RIGHT
            boardNeighbourPointCoordinates = (boardPointXCoordinate + 1) + "," + boardPointYCoordinate;
            addEdgeToGameBoard(boardPointCoordinates, boardNeighbourPointCoordinates, Position.R, userInputBoard, gameBoard);

            // these are the diagonal edges that are disabled by default, but can be included by simply uncommenting the section below

/*            // neighbour UP-LEFT
            boardNeighbourPointCoordinates = (boardPointXCoordinate - 1) + "," + (boardPointYCoordinate + 1);
            addEdgeToGameBoard(boardPointCoordinates, boardNeighbourPointCoordinates, Position.UL, userInputBoard, gameBoard);

            // neighbour DOWN-LEFT
            boardNeighbourPointCoordinates = (boardPointXCoordinate - 1) + "," + (boardPointYCoordinate - 1);
            addEdgeToGameBoard(boardPointCoordinates, boardNeighbourPointCoordinates, Position.DL, userInputBoard, gameBoard);

            // neighbour UP-RIGHT
            boardNeighbourPointCoordinates = (boardPointXCoordinate + 1) + "," + (boardPointYCoordinate + 1);
            addEdgeToGameBoard(boardPointCoordinates, boardNeighbourPointCoordinates, Position.UR, userInputBoard, gameBoard);

            // neighbour DOWN-RIGHT
            boardNeighbourPointCoordinates = (boardPointXCoordinate + 1) + "," + (boardPointYCoordinate - 1);
            addEdgeToGameBoard(boardPointCoordinates, boardNeighbourPointCoordinates, Position.DR, userInputBoard, gameBoard);*/

            // neighbour UP-LEFT-TRIANGLE
            boardNeighbourPointCoordinates = (boardPointXCoordinate - 0.5) + "," + (boardPointYCoordinate + 1);
            addEdgeToGameBoard(boardPointCoordinates, boardNeighbourPointCoordinates, Position.ULT, userInputBoard, gameBoard);

            // neighbour DOWN-LEFT-TRIANGLE
            boardNeighbourPointCoordinates = (boardPointXCoordinate - 0.5) + "," + (boardPointYCoordinate - 1);
            addEdgeToGameBoard(boardPointCoordinates, boardNeighbourPointCoordinates, Position.DLT, userInputBoard, gameBoard);

            // neighbour UP-RIGHT-TRIANGLE
            boardNeighbourPointCoordinates = (boardPointXCoordinate + 0.5) + "," + (boardPointYCoordinate + 1);
            addEdgeToGameBoard(boardPointCoordinates, boardNeighbourPointCoordinates, Position.URT, userInputBoard, gameBoard);

            // neighbour DOWN-RIGHT-TRIANGLE
            boardNeighbourPointCoordinates = (boardPointXCoordinate + 0.5) + "," + (boardPointYCoordinate - 1);
            addEdgeToGameBoard(boardPointCoordinates, boardNeighbourPointCoordinates, Position.DRT, userInputBoard, gameBoard);
        }

        return gameBoard;
    }

    public static void addEdgeToGameBoard(String boardPointCoordinates, String boardNeighbourPointCoordinates, Position neighbourPosition, HashMap<String, Integer> userInputBoard, DirectedSparseMultigraph<Node, Edge> gameBoard) {
        // only want to do anything if the direction we're looking in actually has a neighbour node
        if(userInputBoard.containsKey(boardNeighbourPointCoordinates)) {
            Node currentNode = nodeMap.get(userInputBoard.get(boardPointCoordinates));
            Node neighbourNode = nodeMap.get(userInputBoard.get(boardNeighbourPointCoordinates));

            // add new directed edge from currentNode to neighbourNode with direction of neighbourNode relative to currentNode
            Edge edge = new Edge(neighbourPosition);
            gameBoard.addEdge(edge, currentNode, neighbourNode);
        }
    }

    public static ArrayList<Move> getAllPossibleMovesOnBoard(DirectedSparseMultigraph<Node, Edge> gameBoard) {

        ArrayList<Move> allPossibleMoves = new ArrayList<Move>();

        System.out.println("The possible moves on this board are - ");

        for(Node initialNode : gameBoard.getVertices()) {
            for(Node neighbourNode : gameBoard.getNeighbors(initialNode)) {
                Edge edge = gameBoard.findEdge(initialNode, neighbourNode);
                Position nodePosition = edge.getNodePosition();
                for(Node destinationNode : gameBoard.getNeighbors(neighbourNode)) {
                    Edge destinationEdge = gameBoard.findEdge(neighbourNode, destinationNode);
                    if(destinationEdge.getNodePosition() == nodePosition) {
                        allPossibleMoves.add(new Move(initialNode.getNumber(), neighbourNode.getNumber(), destinationNode.getNumber()));
                        System.out.println("(" + initialNode.getNumber() + "," + neighbourNode.getNumber() + "," + destinationNode.getNumber() + ") : " + nodePosition);
                    }
                }
            }
        }

        return allPossibleMoves;
    }

    public static BoolExpr[][] createBoardStates(DirectedSparseMultigraph<Node, Edge> gameBoard, Context context) throws Exception {
        int numberOfNodes = gameBoard.getVertexCount();

        CSVReader reader = new CSVReader(new FileReader(boardFileName));

        String[] nextLine;
        // arrays for defining the initial and final board states from the input user configuration
        BoolExpr[] initialBoardState = new BoolExpr[numberOfNodes];
        BoolExpr[] finalBoardState = new BoolExpr[numberOfNodes];
        int numberOfInitialHolesWithPegs = 0;
        int numberOfFinalHolesWithPegs = 0;

        userDefinedConfigConstraints = new ArrayList<BoolExpr>();

        int counter = 0;
        while((nextLine = reader.readNext()) != null) {
            boolean hasPegInInitialConfig = Boolean.parseBoolean(nextLine[2]);
            boolean hasPegInFinalConfig = Boolean.parseBoolean(nextLine[3]);

            // make new boolean variable for this hole in initial board state
            initialBoardState[counter] = context.mkBoolConst("Hole " + counter + " in board state 0");
            if(hasPegInInitialConfig == true) {
                numberOfInitialHolesWithPegs++;
                // if user says peg must be there in this hole, include this constraint in our solver
                userDefinedConfigConstraints.add(initialBoardState[counter]);
            } else {
                // if user says peg musn't be there, include that constraint instead
                userDefinedConfigConstraints.add(context.mkNot(initialBoardState[counter]));
            }

            finalBoardState[counter] = context.mkBoolConst("Hole " + counter + " in final board state");
            if(hasPegInFinalConfig == true) {
                numberOfFinalHolesWithPegs++;
                userDefinedConfigConstraints.add(finalBoardState[counter]);
            } else {
                userDefinedConfigConstraints.add(context.mkNot(finalBoardState[counter]));
            }

            counter++;  // don't forget to increment your counter :^)
        }

        // since each state removes a peg, number of transitions we need is the initial no. of pegs we started out with minus the final no. we want to end up with
        // number of states required is no. of transitions + initial state
        int numberOfBoardStatesRequired = (numberOfInitialHolesWithPegs - numberOfFinalHolesWithPegs) + 1;

        BoolExpr[][] boardStates = new BoolExpr[numberOfBoardStatesRequired][numberOfNodes]; // required no. of states of length n each, corresponding to each hole in board
        boardStates[0] = initialBoardState;
        boardStates[boardStates.length-1] = finalBoardState;

        // we've already defined the initial ad final board states above, so iterate through everything in between
        for(int i = 1; i < boardStates.length-1; i++) {
            for(int j = 0; j < boardStates[i].length; j++) {
                boardStates[i][j] = context.mkBoolConst("Hole " + j + " in board state " + i);
            }
        }

        return boardStates;
    }

    // valid(p, q, (x, y, z)), where (x, y, z) is the move from state p to state q
    // uses approach described in http://ai.stanford.edu/~chuongdo/satpage/
    public static BoolExpr createValidMoveExpression(BoolExpr[] initialBoardState, BoolExpr[] finalBoardState, Move move, BoolExpr selectionVariableCombination, Context context) throws  Exception {
        BoolExpr validMoveExpression = context.mkOr(
                selectionVariableCombination,   // selectionVariableCombination is OR'd with each 'AND' component of the 'valid' expression
                context.mkAnd(initialBoardState[move.getInitial()], context.mkNot(finalBoardState[move.getInitial()])) // p_x & !q_x
        );

        // append new expression to the overall one
        validMoveExpression = context.mkAnd(
                validMoveExpression,
                context.mkOr(
                        selectionVariableCombination,
                        context.mkAnd(initialBoardState[move.getNeighbour()], context.mkNot(finalBoardState[move.getNeighbour()]))  // add p_y & !q_y
                )
        );

        validMoveExpression = context.mkAnd(
                validMoveExpression,
                context.mkOr(
                        selectionVariableCombination,
                        context.mkAnd(context.mkNot(initialBoardState[move.getDestination()]), finalBoardState[move.getDestination()])  // add !p_z & q_z
                )
        );

        for(int i = 0; i < initialBoardState.length; i++) {
            if(i == move.getInitial() || i == move.getNeighbour() || i == move.getDestination()) {
                continue;
            }

            // (p iff q) = (!p_i || q_i) & (p_i || !q_i)
            BoolExpr iffExpression = context.mkAnd(
                    context.mkOr(context.mkNot(initialBoardState[i]), finalBoardState[i]),
                    context.mkOr(initialBoardState[i], context.mkNot(finalBoardState[i]))
            );

            validMoveExpression = context.mkAnd(
                    validMoveExpression,
                    context.mkOr(selectionVariableCombination, iffExpression)
            );
        }

        return validMoveExpression;
    }

    public static ArrayList<BoolExpr> generateSelectionVariableCombinations(ArrayList<Move> allPossibleMoves, int initialBoardState, int finalBoardState, Context context) throws Exception {
        int numberOfPossibleMoves = allPossibleMoves.size();    // number of selection variables is determined by the total number of possible moves that can be made
        int numberOfSelectionVariables = logBase2(numberOfPossibleMoves);   // rounded up in custom method to give required number
        ArrayList<BoolExpr> selectionVariables = new ArrayList<BoolExpr>();
        ArrayList<BoolExpr> selectionVariableCombinations = new ArrayList<BoolExpr>();

        // instantiate selection variables for this transition
        for(int i = 0; i < numberOfSelectionVariables; i++) {
            BoolExpr selectionVariable = context.mkBoolConst("Selection variable " + i + " for board state " + initialBoardState + " -> " + finalBoardState);
            selectionVariables.add(selectionVariable);
            // copy the newly declared selection variable into our global array as well
            selectionVariablesArray.add(selectionVariable);
        }

        // number of combinations of n boolean variables : 0 to (2^n) - 1
        for(int i = 0; i < Math.pow(2, numberOfSelectionVariables); i++) {
            // http://stackoverflow.com/questions/1316765/what-is-an-efficient-algorithm-to-create-all-possible-combinations
            // http://stackoverflow.com/questions/8457734/pad-a-binary-string-equal-to-zero-0-with-leading-zeros-in-java
            String booleanCombination = String.format("%" + numberOfSelectionVariables + "s", Integer.toBinaryString(i)).replace(' ', '0');
            BoolExpr selectionVariableCombination = context.mkFalse();  // need to initialise variable to something, make it false since (X v F) = X
            // go through each bit of the generated combination
            for(int j = 0; j < booleanCombination.length(); j++) {
                // get the selection variable corresponding to the current bit
                BoolExpr selectionVariable = selectionVariables.get(j);
                int bitValue = Character.getNumericValue(booleanCombination.charAt(j));
                // if bit in current combination is 0, use the boolean variable, else use the negation
                if(bitValue == 0) {
                    selectionVariableCombination = context.mkOr(selectionVariableCombination, selectionVariable);
                } else if(bitValue == 1) {
                    selectionVariableCombination = context.mkOr(selectionVariableCombination, context.mkNot(selectionVariable));
                }
            }
            selectionVariableCombinations.add(selectionVariableCombination);
        }

        return selectionVariableCombinations;
    }

    public static BoolExpr createLegalTransitionExpression(int initialBoardStateNumber, int finalBoardStateNumber, BoolExpr[] initialBoardState, BoolExpr[] finalBoardState, ArrayList<Move> allPossibleMoves, Context context) throws Exception {
        // generate the selection variable combinations for this board transition
        ArrayList<BoolExpr> selectionVariableCombinations = generateSelectionVariableCombinations(allPossibleMoves, initialBoardStateNumber, finalBoardStateNumber, context);

        BoolExpr legalTransitionExpression = context.mkTrue();  // (X and T) = X


        for(int i = 0; i < allPossibleMoves.size(); i++) {
            Move move = allPossibleMoves.get(i);
            // choose one unique selection variable to associate with the current 'valid' expression
            BoolExpr selectionVariableCombination = selectionVariableCombinations.get(i);
            legalTransitionExpression = context.mkAnd(legalTransitionExpression, createValidMoveExpression(initialBoardState, finalBoardState, move, selectionVariableCombination, context));
        }

        // all the unused selection variable combinations need to be included in expression as well
        for(int i = allPossibleMoves.size(); i < selectionVariableCombinations.size(); i++) {
            legalTransitionExpression = context.mkAnd(legalTransitionExpression, selectionVariableCombinations.get(i));
        }

        return legalTransitionExpression;
    }

    public static int logBase2(double number) {
        return (int) Math.ceil(Math.log(number)/Math.log(2));
    }
}